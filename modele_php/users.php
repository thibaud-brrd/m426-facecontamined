<?php

require_once 'connexion.php';

// Create
// ------------------------------------------------------------------------------

function addUser($login, $email, $password, $confirmePassword, $firstname, $lastname) {
    if ($password == $confirmePassword) {
    $password = hash('sha256', $password);
    $idStatus = 1;
    $idEtat = 1;

    $connexion = connectDB();
    $requete = $connexion->prepare(
        "INSERT INTO `t_users` ( `login`, `email`, `password`, `firstname`, `lastname`, `idStatus`, `idEtat`)
        VALUES (:login, :email, :password, :firstname, :lastname, :idStatus, :idEtat)");
    $requete->bindParam('login', $login, PDO::PARAM_STR, 50);
    $requete->bindParam('email', $email, PDO::PARAM_STR, 50);
    $requete->bindParam('password', $password, PDO::PARAM_STR, 64);
    $requete->bindParam('firstname', $firstname, PDO::PARAM_STR, 50);
    $requete->bindParam('lastname', $lastname, PDO::PARAM_STR, 50);
    $requete->bindParam('idStatus', $idStatus, PDO::PARAM_INT, 10);
    $requete->bindParam('idEtat', $idEtat, PDO::PARAM_INT, 10);
    $requete->execute();
    return $connexion->lastInsertId();
    }
    else {
        return false;
    }
}

// ------------------------------------------------------------------------------

// Request
// ------------------------------------------------------------------------------

function getUser($login) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `u`.`login`, `s`.`label`
    FROM `t_users` as u
    LEFT JOIN `t_status` as s
    ON `u`.`idStatus` = `s`.`id`
    WHERE `u`.`login` = :login");
    $requete->bindParam('login', $login, PDO::PARAM_STR, 50);
    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);
}

function getUserInformation($login) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `u`.`login`, `u`.`email`, `u`.`firstname`, `u`.`lastname`
    FROM `t_users` as u
    WHERE `u`.`login` = :login");
    $requete->bindParam('login', $login, PDO::PARAM_STR, 50);
    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);
}

function getEtatOfUser($login) {

    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `s`.`label` as 'label'
    FROM `t_users` as u
    LEFT JOIN `t_etats` as s
    ON `u`.`idEtat` = `s`.`id`
    WHERE `u`.`login` = :login");
    $requete->bindParam('login', $login, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]['label'];
}

function getAllUsers($noPage, $maxElementPerPage) {
    $firstOfPage = ($noPage - 1) * $maxElementPerPage;

    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `u`.`login`, `s`.`label`
    FROM `t_users` as u
    LEFT JOIN `t_status` as s
    ON `u`.`idStatus` = `s`.`id`
    LIMIT :firstOfPage, :maxElementPerPage");
    $requete->bindParam('firstOfPage', $firstOfPage, PDO::PARAM_INT, 100);
    $requete->bindParam('maxElementPerPage', $maxElementPerPage, PDO::PARAM_INT, 100);
    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);
}

function getNumberOfUsers() {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_users`");
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function getNumberOfUsersWithSameEtat($idEtat) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_users`
    WHERE `idEtat` = :idEtat");
    $requete->bindParam('idEtat', $idEtat, PDO::PARAM_INT, 10);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function getNumberOfUsersWithSameStatus($idStatus) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_users`
    WHERE `idStatus` = :idStatus");
    $requete->bindParam('idStatus', $idStatus, PDO::PARAM_INT, 10);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function checkUserIdentity($login, $password) {
    $password = hash('sha256', $password);
    
    $connexion = connectDB();
    $requete = $connexion->prepare(
        "SELECT `login`, `password`
        FROM `t_users`
        WHERE `login` = :login");
    $requete->bindParam('login', $login, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);

    if ($requete[0]['password'] == $password) {
        return true;
    }
    else {
        return false;
    }
}

// ------------------------------------------------------------------------------

// Update
// ------------------------------------------------------------------------------

//function updateUser($id, $login, $password, $confirmePassword, $firstname, $lastname) {
//
//    if (true) {
//    $password = hash('sha256', $password);
//
//    $db = connectDB();
//    $sql = "UPDATE `t_users` SET "
//            . "lastName = :lastName, "
//            . "firstName = :firstName, "
//            . "email = :email "
//            . "WHERE id = :id ";
//
//    $request = $db->prepare($sql);
//    if ($request->execute(array(
//                'lastName' => $lastName,
//                'firstName' => $firstName,
//                'email' => $email,
//                'id' => $idUser))) {
//        return $idUser;
//    } else {
//        return NULL;
//    }
//}
//
//// ------------------------------------------------------------------------------
//
// Delete
// ------------------------------------------------------------------------------

function deleteUser($id) {
    $db = connectDB();
    $sql = "DELETE FROM t_users WHERE `id` = :id";
    $request = $db->prepare($sql);
    return ($request->execute(array('id' => $id)));
}

// ------------------------------------------------------------------------------