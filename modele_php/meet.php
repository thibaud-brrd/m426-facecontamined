<?php

require_once 'connexion.php';

// Create
// ------------------------------------------------------------------------------

function addMeet($idUser, $idFriendUser, $startDate, $endDate) {

    $connexion = connectDB();
    $requete = $connexion->prepare(
        "INSERT INTO `t_users` ( `idUser`, `isFriendUser`, `startDate`, `endDate`)
        VALUES (:idUser, :idFriendUser, :startDate, :endDate)");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->bindParam('idFriendUser', $idFriendUser, PDO::PARAM_STR, 50);
    $requete->bindParam('startDate', $startDate, PDO::PARAM_STR, 30);
    $requete->bindParam('endDate', $endDate, PDO::PARAM_STR, 30);
    $requete->execute();
    return $connexion->lastInsertId();
}

// ------------------------------------------------------------------------------

// Request
// ------------------------------------------------------------------------------

function getAllMeetOfUserOrderedByColumn($idUser, $column) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `m`.`idUser`, `m`.`idFriendUser`, `m`.`startDate`, `m`.`endDate`
    FROM `t_meet` as m
    WHERE `m`.`idUser` = :idUser
    ORDER BY :column");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->bindParam('column', $column, PDO::PARAM_STR, 50);
    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);
}

function getDateOfFirstMeet($idUser) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT min(`m`.`startDate`) as 'firstDate'
    FROM `t_meet` as m
    WHERE `m`.`idUser` = :idUser");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]['firstDate'];
}

function getDateOfLastMeet($idUser) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT MAX(`m`.`startDate`) as 'lastDate'
    FROM `t_meet` as m
    WHERE `m`.`idUser` = :idUser");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]['lastDate'];
}

function getDateOfLastRiskyMeet($idUser) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT MAX(`m`.`startDate`) as 'lastRiskyDate'
    FROM `t_meet` as m
    LEFT JOIN `t_users` as u
    ON `m`.`idFriendUser` = `u`.`login`
    WHERE `m`.`idUser` = :idUser
    AND `u`.`idEtat` = 2
    OR `u`.`idEtat` = 3");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]['lastRiskyDate'];
}

function getNumberOfMeet() {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_meet` as m");
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function getNumberOfMeetWithSameEtat($idEtat) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_meet` as m
    LEFT JOIN `t_users` as u
    ON `m`.`idFriendUser` = `u`.`login`
    WHERE `u`.`idEtat` = :idEtat");
    $requete->bindParam('idEtat', $idEtat, PDO::PARAM_INT, 10);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function getNumberOfRiskyMeet() {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_meet` as m
    LEFT JOIN `t_users` as u
    ON `m`.`idFriendUser` = `u`.`login`
    WHERE `u`.`idEtat` = 2
    OR `u`.`idEtat` = 3");
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}