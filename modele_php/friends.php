<?php

require_once 'connexion.php';

// Create
// ------------------------------------------------------------------------------

function addFriend($idUser, $idFriendUser) {
    if ($idUser != $idFriendUser) {
        $connexion = connectDB();
        $requete = $connexion->prepare(
            "INSERT INTO `t_friends` ( `idUser`, `idFriendUser`)
            VALUES (:idUser, :idFriendUser)");
        $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
        $requete->bindParam('idFriend', $idFriend, PDO::PARAM_STR, 50);
        $requete->execute();

        return true;
    }
    else {
        return false;
    }
}

// ------------------------------------------------------------------------------

// Request
// ------------------------------------------------------------------------------

function getAllFriends($idUser, $noPage, $maxElementPerPage) {
    $firstOfPage = ($noPage - 1) * $maxElementPerPage;

    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT `u`.`lastname`, `u`.`firstname`, `u`.`login`, `e`.`label` as 'etat', `s`.`label` as 'status'
    FROM `t_friends` as f
    LEFT JOIN `t_users` as u
    ON `f`.`idFriendUser` = `u`.`login`
    LEFT JOIN `t_etats` as e
    ON `u`.`idEtat` = `e`.`id`
    LEFT JOIN `t_status` as s
    ON `u`.`idStatus` = `s`.`id`
    WHERE `f`.`idUser` = :idUser
    LIMIT :firstOfPage, :maxElementPerPage");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->bindParam('firstOfPage', $firstOfPage, PDO::PARAM_INT, 100);
    $requete->bindParam('maxElementPerPage', $maxElementPerPage, PDO::PARAM_INT, 100);
    $requete->execute();
    return $requete->fetchAll(PDO::FETCH_ASSOC);
}

function getNumberOfFriends($idUser) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_friends` as f
    WHERE `f`.`idUser` = :idUser");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}

function getNumberOfFriendsWithSameEtat($idUser, $idEtat) {
    $connexion = connectDB();
    $requete = $connexion->prepare(
    "SELECT count(*)
    FROM `t_friends` as f
    LEFT JOIN `t_users` as u
    ON `f`.`idFriendUser` = `u`.`login`
    WHERE `f`.`idUser` = :idUser
    AND `u`.`idEtat` = :idEtat");
    $requete->bindParam('idUser', $idUser, PDO::PARAM_STR, 50);
    $requete->bindParam('idEtat', $idEtat, PDO::PARAM_INT, 10);
    $requete->execute();
    $requete = $requete->fetchAll(PDO::FETCH_ASSOC);
    return $requete[0]["count(*)"];
}