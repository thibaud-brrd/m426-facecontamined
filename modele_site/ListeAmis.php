<?php

// Call the CRUD php scripts
require_once '../modele_php/users.php';
require_once '../modele_php/friends.php';
require_once '../modele_php/display.php';
// Call the session init php script
require_once 'session_init.php';
// Create local var from the session
$me = $_SESSION['me'];

if($me != "")
{
    echo("It's $me");

    $cardProfil = [
        'value' => "$me"
    ];

    $tableFriends = getAllFriends($me, 1, 10);

    $linkPrimary = [
        'label' => "Mon profil",
        'link' => "profile.php"
    ];
    $linkSecondary = [
        'label' => "Ma liste d'amis",
        'link' => "listeAmis.php"
    ];
}
else {
    header('Location: connection.php');
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Table - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Animated-CSS-Waves-Background-SVG.css">
    <link rel="stylesheet" href="assets/css/Animated-rainbow-shadow.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/The-Matrix-Display.css">
    <link rel="stylesheet" href="assets/css/untitled.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
            <div class="container-fluid d-flex flex-column p-0">
                <hr class="sidebar-divider my-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#" style="opacity: 1;">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laugh-wink"></i></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 12px;">Face Contaminated</span></div>
                </a>
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="index.php"><i class="fas fa-tachometer-alt"></i><span>Acceuil</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href=<?= $linkPrimary['link'] ?>><i class="fas fa-user"></i><span><?= $linkPrimary['label'] ?></span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href=<?= $linkSecondary['link'] ?>><i class="fas fa-table"></i><span><?= $linkSecondary['label'] ?></span></a></li>
                    <li class="nav-item" role="presentation"></li>
                    <li class="nav-item" role="presentation"></li>
                    <li class="nav-item" role="presentation"></li>
                    <li class="nav-item" role="presentation"></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <div class="container-fluid">
                    <div class="card shadow">
                        <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                            <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                                <form class="form-inline d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                                    <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Trouver des amis...">
                                        <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                    </div>
                                </form>
                                <ul class="nav navbar-nav flex-nowrap ml-auto">
                                    <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                                            <form class="form-inline mr-auto navbar-search w-100">
                                                <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                                    <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown no-arrow mx-1" role="presentation">
                                        <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class="badge badge-danger badge-counter">3+</span><i class="fas fa-bell fa-fw"></i></a>
                                            <div class="dropdown-menu dropdown-list dropdown-menu-right animated--grow-in"
                                                role="menu">
                                                <h6 class="dropdown-header">alerts center</h6>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="mr-3">
                                                        <div class="bg-primary icon-circle"><i class="fas fa-file-alt text-white"></i></div>
                                                    </div>
                                                    <div><span class="small text-gray-500">December 12, 2019</span>
                                                        <p>A new monthly report is ready to download!</p>
                                                    </div>
                                                </a>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="mr-3">
                                                        <div class="bg-success icon-circle"><i class="fas fa-donate text-white"></i></div>
                                                    </div>
                                                    <div><span class="small text-gray-500">December 7, 2019</span>
                                                        <p>$290.29 has been deposited into your account!</p>
                                                    </div>
                                                </a>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="mr-3">
                                                        <div class="bg-warning icon-circle"><i class="fas fa-exclamation-triangle text-white"></i></div>
                                                    </div>
                                                    <div><span class="small text-gray-500">December 2, 2019</span>
                                                        <p>Spending Alert: We've noticed unusually high spending for your account.</p>
                                                    </div>
                                                </a><a class="text-center dropdown-item small text-gray-500" href="#">Show All Alerts</a></div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown no-arrow mx-1" role="presentation">
                                        <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-envelope fa-fw"></i><span class="badge badge-danger badge-counter">7</span></a>
                                            <div class="dropdown-menu dropdown-list dropdown-menu-right animated--grow-in"
                                                role="menu">
                                                <h6 class="dropdown-header">alerts center</h6>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="dropdown-list-image mr-3"><img class="rounded-circle" src="assets/img/photo-1458071103673-6a6e4c4a3413.jpeg">
                                                        <div class="bg-success status-indicator"></div>
                                                    </div>
                                                    <div class="font-weight-bold">
                                                        <div class="text-truncate"><span>Hi there! I am wondering if you can help me with a problem I've been having.</span></div>
                                                        <p class="small text-gray-500 mb-0">Emily Fowler - 58m</p>
                                                    </div>
                                                </a>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="dropdown-list-image mr-3"><img class="rounded-circle" src="assets/img/photo-1531550138977-3c1c4baee57b.jpeg">
                                                        <div class="status-indicator"></div>
                                                    </div>
                                                    <div class="font-weight-bold">
                                                        <div class="text-truncate"><span>I have the photos that you ordered last month!</span></div>
                                                        <p class="small text-gray-500 mb-0">Jae Chun - 1d</p>
                                                    </div>
                                                </a>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="dropdown-list-image mr-3"><img class="rounded-circle" src="assets/img/photo-1531483789621-6dc42dfa5078.jpeg">
                                                        <div class="bg-warning status-indicator"></div>
                                                    </div>
                                                    <div class="font-weight-bold">
                                                        <div class="text-truncate"><span>Last month's report looks great, I am very happy with the progress so far, keep up the good work!</span></div>
                                                        <p class="small text-gray-500 mb-0">Morgan Alvarez - 2d</p>
                                                    </div>
                                                </a>
                                                <a class="d-flex align-items-center dropdown-item" href="#">
                                                    <div class="dropdown-list-image mr-3"><img class="rounded-circle" src="assets/img/photo-1517849845537-4d257902454a.jpeg">
                                                        <div class="bg-success status-indicator"></div>
                                                    </div>
                                                    <div class="font-weight-bold">
                                                        <div class="text-truncate"><span>Am I a good boy? The reason I ask is because someone told me that people say this to all dogs, even if they aren't good...</span></div>
                                                        <p class="small text-gray-500 mb-0">Chicken the Dog · 2w</p>
                                                    </div>
                                                </a><a class="text-center dropdown-item small text-gray-500" href="#">Show All Alerts</a></div>
                                        </div>
                                        <div class="shadow dropdown-list dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown"></div>
                                    </li>
                                    <div class="d-none d-sm-block topbar-divider"></div>
                                    <li class="nav-item dropdown no-arrow" role="presentation">
                                        <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class="d-none d-lg-inline mr-2 text-gray-600 small"><?= $cardProfil['value'] ?></span><img class="border rounded-circle img-profile" src="assets/img/avatars/userDefault.png"></a>
                                            <div
                                                class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Profile</a><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Settings</a>
                                        <a
                                            class="dropdown-item" role="presentation" href="#"><i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Activity log</a>
                                        <div class="dropdown-divider"></div><a class="dropdown-item" role="presentation" href="#"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="card-header py-3">
                        <p class="text-primary m-0 font-weight-bold">Liste d'amis</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 text-nowrap">
                                <div id="dataTable_length" class="dataTables_length" aria-controls="dataTable"><label>Montré<select class="form-control form-control-sm custom-select custom-select-sm"><option value="10" selected="">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select>&nbsp;</label></div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-md-right dataTables_filter" id="dataTable_filter"><label><input type="search" class="form-control form-control-sm" aria-controls="dataTable" placeholder="recherche" /></label></div>
                            </div>
                        </div>
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <?= displayQueryAsTable($tableFriends) ?>
                        </div>
                        <div class="row">
                            <div class="col-md-6 align-self-center">
                                <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Montré 1 a 10 sur 27</p>
                            </div>
                            <div class="col-md-6">
                                <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                                    <ul class="pagination">
                                        <li class="page-item disabled"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright ©</span></div>
            </div>
        </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/The-Matrix-Display.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>