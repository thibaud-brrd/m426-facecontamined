<?php

// Call the CRUD php scripts
require_once '../modele_php/users.php';
// Call the session init php script
require_once 'session_init.php';
// Create local var from the session
$me = $_SESSION['me'];

$login = filter_input(INPUT_POST, "login", FILTER_SANITIZE_STRING);
$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$btn = filter_input(INPUT_POST,"submit");

if($btn == "connexion")
{
    if(checkUserIdentity($login, $password)){
        $me = $login;
        $_SESSION['me'] = $me;
        header('Location: profile.php');
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - Brand</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Animated-CSS-Waves-Background-SVG.css">
    <link rel="stylesheet" href="assets/css/Animated-rainbow-shadow.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/The-Matrix-Display.css">
    <link rel="stylesheet" href="assets/css/untitled.css">
</head>

<body class="bg-gradient-primary">
    <div class="login-dark">
        <form method="post">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group"><input class="form-control" type="text" name="login" placeholder="Pseudo"></div>
            <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Mot de passe"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="submit" value="connexion">Connection</button></div><a class="forgot" href="#">Mot de passe oublié ??</a></form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/The-Matrix-Display.js"></script>
    <script src="assets/js/theme.js"></script>
</body>

</html>