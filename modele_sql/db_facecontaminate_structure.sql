-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 04 juin 2020 à 15:03
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_facecontaminate`
--
CREATE DATABASE IF NOT EXISTS `db_facecontaminate` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_facecontaminate`;

-- --------------------------------------------------------

--
-- Structure de la table `t_contaminationperiod`
--

CREATE TABLE `t_contaminationperiod` (
  `id` int(10) NOT NULL,
  `idUser` varchar(50) NOT NULL,
  `idVirus` int(10) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_etats`
--

CREATE TABLE `t_etats` (
  `id` int(10) NOT NULL,
  `label` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_etats`
--

INSERT INTO `t_etats` (`id`, `label`) VALUES
(2, 'risky'),
(1, 'safe'),
(3, 'contaminated'),
(4, 'unknown');

-- --------------------------------------------------------

--
-- Structure de la table `t_friends`
--

CREATE TABLE `t_friends` (
  `idUser` varchar(50) NOT NULL,
  `idFriendUser` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_meet`
--

CREATE TABLE `t_meet` (
  `id` int(10) NOT NULL,
  `idUser` varchar(50) NOT NULL,
  `idFriendUser` varchar(50) NOT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_status`
--

CREATE TABLE `t_status` (
  `id` int(10) NOT NULL,
  `label` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_status`
--

INSERT INTO `t_status` (`id`, `label`) VALUES
(2, 'disconnected'),
(1, 'connected');

-- --------------------------------------------------------

--
-- Structure de la table `t_users`
--

CREATE TABLE `t_users` (
  `login` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `idStatus` int(10) NOT NULL DEFAULT '0',
  `idEtat` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_virus`
--

CREATE TABLE `t_virus` (
  `id` int(10) NOT NULL,
  `label` varchar(30) NOT NULL,
  `incubationDays` int(3) NOT NULL,
  `contaminateDuringIncubation` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_contaminationperiod`
--
ALTER TABLE `t_contaminationperiod`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_etats`
--
ALTER TABLE `t_etats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_friends`
--
ALTER TABLE `t_friends`
  ADD PRIMARY KEY (`idUser`,`idFriendUser`);

--
-- Index pour la table `t_meet`
--
ALTER TABLE `t_meet`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_status`
--
ALTER TABLE `t_status`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`login`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Index pour la table `t_virus`
--
ALTER TABLE `t_virus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_contaminationperiod`
--
ALTER TABLE `t_contaminationperiod`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_etats`
--
ALTER TABLE `t_etats`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_meet`
--
ALTER TABLE `t_meet`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_status`
--
ALTER TABLE `t_status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_virus`
--
ALTER TABLE `t_virus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
